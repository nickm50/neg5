"use strict";

$(document).ready(function() {
    $("#loginfo").submit(function(event, done) {
        checkEmail();
    });
});

function checkEmail() {
    var email = document.forms["logins"]["usrname"].value;
    console.log(email);
    if (email.length != 0) {
        getLogin(email);
    }
}

function getLogin(email) {
    $.post("/login", {"usrname" : email});
}
